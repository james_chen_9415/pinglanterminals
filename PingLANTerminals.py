## Auther: James Chen
## Final Modify: 31/08/2016
## Email: james.chen.9415@gmail.com

import os
import re
import time
import sys
import subprocess
import threading


## class for each CMD ping
class PingObject:

   lifeline = re.compile(r" bytes=")
   
   ##def __init__(self):
      
   def __del__(self):
      class_name = self.__class__.__name__
      #print class_name, "destroy"

   def ping(self, ip):
      pingaling = subprocess.Popen(["ping",ip ,"-n", "1"], shell=False, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
      result = "Testing " + ip
      while 1:
         pingaling.stdout.flush()
         line = pingaling.stdout.readline()
         if not line: break
         igot = re.findall(self.lifeline,line)
         alive_flag = False
         if igot:
            result += " Alive"
            alive_flag = True
            break
      if not alive_flag:
         result += " No response"
      summary.append(result)
      

class myThread (threading.Thread):   #inherit threading.Thread
   def __init__(self, ip):
      threading.Thread.__init__(self)
      self.ip = ip
   def run(self):                   #main part 
        #print "Starting " + self.name
      obj = PingObject()
      obj.ping(self.ip)
        #print "Exiting " + self.name


def lookfor(thirdNum):
   print time.ctime()

   ## choose IP addresses, create threads
   for num1 in range(256):
      ip = "169.254." + str(thirdNum) + "." + str(num1)
      threads.append(myThread(ip))

   ## start to process(ping)
   for i in range(len(threads)):
      threads[i].start()

   ## waiting for all of threads finishing
   for i in range(len(threads)):
     threading.Thread.join(threads[i])

   ## write result to file
   fo = open("ipaddress.txt", "wb")
   for result in summary:
      fo.write(result + '\r\n');
##      print result
   fo.close()

   print time.ctime()


def main():
   for num in range(256):
      lookfor(num)
      threads = []

################################## program entry ####################################
summary = []
threads = []

if __name__ == '__main__':
    main()
